package com.example.kadern.samples2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.niallkader.Coder;

public class CoderActivity extends AppCompatActivity {

    EditText txtMessage;
    Button btnEncrypt;
    Button btnDecrypt;
    Coder coder = new Coder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coder);
        txtMessage = (EditText)findViewById(R.id.txtMessage);
        btnEncrypt = (Button)findViewById(R.id.btnEncrypt);
        btnDecrypt = (Button)findViewById(R.id.btnDecrypt);

        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = txtMessage.getText().toString();
                String encryptedMsg = coder.encode(msg);
                txtMessage.setText(encryptedMsg);
            }
        });

        btnDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = txtMessage.getText().toString();
                String decryptedMsg = coder.decode(msg);
                txtMessage.setText(decryptedMsg);
            }
        });
    }
}
