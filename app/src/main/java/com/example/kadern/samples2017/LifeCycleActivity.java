package com.example.kadern.samples2017;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.kadern.samples2017.models.User;

public class LifeCycleActivity extends AppCompatActivity {

    public static final String TAG = "LifeCycleActivity";

    User user = new User();
    Button btnIntent;
    Button btnSave;
    EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_cycle);

        if (savedInstanceState != null) {
            user.setFirstName(savedInstanceState.getString("FIRST_NAME"));
        }


        Log.d(TAG, "OnCreate()......" + user.getFirstName());

        btnIntent = (Button) findViewById(R.id.btnIntent);
        btnIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://www.google.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        btnSave = (Button) findViewById(R.id.btnSave);

        editText =(EditText) findViewById(R.id.editText);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "OnResume()......" + user.getFirstName());

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.d(TAG, "OnRestart()......" + user.getFirstName());

    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d(TAG, "OnStop()......" + user.getFirstName());
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d(TAG, "OnPause()......" + user.getFirstName());

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState().......");
        outState.putString("FIRST_NAME", user.getFirstName());
    }



}

