package com.example.kadern.samples2017.models;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Created by kadern on 9/18/2017.
 */

public class User implements Serializable {
    public enum Music{COUNTRY, RAP, JAZZ};

    private String firstName;
    private String email;
    private Music favoriteMusic;
    private boolean active;
    private long id;

    public User(){
        
    }

    public User(long id, String firstName, String email, Music favoriteMusic, boolean active) {
        this.id = id;
        this.firstName = firstName;
        this.email = email;
        this.favoriteMusic = favoriteMusic;
        this.active = active;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Music getFavoriteMusic() {
        return favoriteMusic;
    }

    public void setFavoriteMusic(Music favoriteMusic) {
        this.favoriteMusic = favoriteMusic;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }


    @Override
    public String toString(){
        return String.format("ID: %d FName: %s Email: %s Music: %s Active: %b", id, firstName, email, favoriteMusic, active);
    }
}
