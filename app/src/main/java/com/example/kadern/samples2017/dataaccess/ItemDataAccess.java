package com.example.kadern.samples2017.dataaccess;

import android.content.ClipData;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.kadern.samples2017.MySQLiteOpenHelper;
import com.example.kadern.samples2017.models.Item;

import java.util.ArrayList;

/**
 * Created by 2822647 on 11/13/2017.
 */

public class ItemDataAccess {

    public static final String TAG = "ItemDataAccess";

    // We'll extend SQLiteHelper and customize it to create our database
    // We'll also use it to give us the SQLiteDatabase object that
    // represents our database
    private MySQLiteOpenHelper dbHelper;
    private SQLiteDatabase database;

    // We should create static constants for the table name, and all column names
    public static final String TABLE_NAME = "items";
    public static final String COLUMN_ITEM_ID = "_id";
    public static final String  COLUMN_ITEM_NAME = "item_name";

    // This is the sql statement to create the table (it will be used by dbHelper)
    public static final String TABLE_CREATE =
            String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT)",
                    TABLE_NAME,
                    COLUMN_ITEM_ID,
                    COLUMN_ITEM_NAME
            );

    public ItemDataAccess(MySQLiteOpenHelper dbHelper){
        // The dbHelper object will be used to get the object that represents our database
        this.dbHelper = dbHelper;
        this.database = this.dbHelper.getWritableDatabase();
    }

    public Item insertItem(Item i){
        ContentValues values = new ContentValues();
        values.put(COLUMN_ITEM_NAME, i.getName());
        long insertId = database.insert(TABLE_NAME, null, values);
        // note: insertId will be -1 if the insert failed
        i.setId(insertId);
        return i;
    }

    public ArrayList<Item> getAllItems(){

        ArrayList<Item> items = new ArrayList<>();
        String query = String.format("SELECT %s, %s FROM %s", COLUMN_ITEM_ID, COLUMN_ITEM_NAME, TABLE_NAME);

        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){
            Item i = new Item(c.getLong(0), c.getString(1));
            items.add(i);
            c.moveToNext(); // DONT FORGET THIS LINE!!!!!!
        }
        c.close();
        return items;

    }

    public Item updateItem(Item i){
        ContentValues values = new ContentValues();
        values.put(COLUMN_ITEM_ID, i.getId());
        values.put(COLUMN_ITEM_NAME, i.getName());

        int rowsUpdated = database.update(TABLE_NAME, values, "_id = " + i.getId(), null);
        // this method returns the number of rows that were updated in the db
        // so that you could use it to confirm that your update worked
        return i;
    }

    public int deleteItem(Item i){
        int rowsDeleted = database.delete(TABLE_NAME, COLUMN_ITEM_ID + " = " + i.getId(), null);
        // the above method returns the number of row that were deleted
        return rowsDeleted;
    }
}
