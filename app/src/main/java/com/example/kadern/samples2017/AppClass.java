package com.example.kadern.samples2017;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.example.kadern.samples2017.dataaccess.UserDataAccess;
import com.example.kadern.samples2017.fileIO.FileIO;
import com.example.kadern.samples2017.models.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by 2822647 on 10/23/2017.
 */

public class AppClass extends Application {

    public static final String TAG = "AppClass";
    public static final String USERS_FILE = "users.dat";
    public String someGlobalVariable = "HELLO THERE";
    public static final FileIO IO = new FileIO();
    public static UserDataAccess userDa;


    public static ArrayList<User> users = new ArrayList<User>();

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize the User Data Access class with the MySQLiteOpenHelper
        userDa = new UserDataAccess(new MySQLiteOpenHelper(this));
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    public void writeUsersToFile(ArrayList<User> users, String filePath) {
        try {
            FileOutputStream fos = openFileOutput(filePath, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(users);
            oos.close();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File Not Found:\n" + e);
        } catch (IOException e) {
            Log.e(TAG, "IO Error:\n" + e);
        } catch (Exception e) {
            Log.e(TAG, "Error:\n" + e);
        }
    }

    public ArrayList<User> readUsersFromFile(String filePath) {

        ArrayList<User> users = new ArrayList<User>();

        try {
            FileInputStream is = openFileInput(filePath);
            ObjectInputStream ois = new ObjectInputStream(is);
            users = (ArrayList<User>)ois.readObject();
            ois.close();
            is.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File Not Found:\n" + e);
        } catch (IOException e) {
            Log.e(TAG, "IO Error:\n" + e);
        } catch (Exception e) {
            Log.e(TAG, "Error:\n" + e);
        }

        return users;
    }


    @Nullable
    public static User getUser(long id) {
        for(User u : users) {
            if(u.getId() == id) {
                return u;
            }
        }
        return null;
    }


    public static int getUserIndexById(long id) {
        for(int i = 0; i < users.size(); i++) {
            if(id == users.get(i).getId()) {
                return i;
            }
        }
        return -1;
    }


    public static void updateUser(long id, User newUser) {
        users.set(getUserIndexById(id), newUser);
    }


    public static void deleteUser(long id) {
        users.remove(getUserIndexById(id));
    }


    public static void addUser(User newUser) {
        users.add(newUser);
    }



}
