package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kadern.samples2017.dataaccess.UserDataAccess;
import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;

public class UserListActivity extends AppCompatActivity {

    // Intent Extra Tags
    public static final String USER_ID_EXTRA = "user_id";
    public static final String DETAILS_MODE = "mode";

    AppClass app;
    Button btnAddUser;
    Button btnDeleteUsers;
    ListView userListView;
    ArrayList<User> users;
    ArrayList<Long> checkedItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        checkedItems = new ArrayList<Long>();

        // Reference the User Interface
        userListView = (ListView)findViewById(R.id.userListView);
        btnAddUser = (Button)findViewById(R.id.btnAddUser);
        btnDeleteUsers = (Button)findViewById(R.id.btnDeleteUsers);

        // Get All Users from the App's UserDataAccess
        users = app.userDa.getAllUsers();

        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, users){
            @Override
            public View getView(final int position, View view, ViewGroup parent){
                View listItemView = super.getView(position, view, parent);

                User currentUser = users.get(position);
                listItemView.setTag(currentUser);

                // get a handle on the widgets inside of this custome_user_list_item layout
                TextView lblFirstName = (TextView)listItemView.findViewById(R.id.lblFirstName);
                CheckBox chkActive = (CheckBox)listItemView.findViewById(R.id.chkActive);

                lblFirstName.setText(currentUser.getFirstName());
                chkActive.setTag(currentUser.getId());

                listItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    User selectedUser = (User)view.getTag();
                    Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                    i.putExtra(USER_ID_EXTRA, selectedUser.getId());
                    i.putExtra(DETAILS_MODE, "edit");
                    startActivity(i);
                    }
                });

                chkActive.setOnClickListener(new CustomerClickListeners());

                return listItemView;
            }
        };

        userListView.setAdapter(adapter);

        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                i.putExtra(DETAILS_MODE, "new");
                startActivity(i);
            }
        });

        btnDeleteUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Long item : checkedItems) {
                    app.userDa.deleteUser(item);
                }

                // Refresh the User List
                Intent i = new Intent(UserListActivity.this, UserListActivity.class);
                startActivity(i);
            }
        });

    }

    private class CustomerClickListeners implements View.OnClickListener {
        public void onClick(View view) {
            CheckBox cbx = (CheckBox)view;
            Long id = (Long)view.getTag();
            if (cbx.isChecked()) {
                checkedItems.add((Long)view.getTag());
            } else {
                for (Long item : checkedItems) {
                    if (item == id) {
                        checkedItems.remove(item);
                    }
                }
            }
        }
    }
}

