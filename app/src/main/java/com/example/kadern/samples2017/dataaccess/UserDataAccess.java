package com.example.kadern.samples2017.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.kadern.samples2017.MySQLiteOpenHelper;
import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;

/**
 * Created by William on 11/14/2017.
 */

public class UserDataAccess {

    public static final String TAG = "UserDataAccess";

    // Static Constant Variables for the table structure
    public static final String TABLE_NAME = "users";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_FAVORITE_MUSIC = "favorite_music";
    public static final String COLUMN_ACTIVE = "active";

    // Static Constant query string for creating the table
    public static final String TABLE_CREATE = String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "%s TEXT, %s TEXT, %s INTEGER, %s INTEGER)",
            TABLE_NAME, COLUMN_USER_ID, COLUMN_FIRST_NAME, COLUMN_EMAIL,
            COLUMN_FAVORITE_MUSIC, COLUMN_ACTIVE);

    // Instance variable for the database connection
    private SQLiteDatabase database;

    // Default Constructor
    public UserDataAccess(MySQLiteOpenHelper dbHelper){
        this.database = dbHelper.getWritableDatabase();
    }


    // CRUD Methods
    public User getUser(long id) {
        Cursor c = database.rawQuery(String.format("SELECT %s, %s, %s, %s, %s FROM %s " +
                        "WHERE %s = %s",
                COLUMN_USER_ID, COLUMN_FIRST_NAME, COLUMN_EMAIL,
                COLUMN_FAVORITE_MUSIC, COLUMN_ACTIVE, TABLE_NAME,
                COLUMN_USER_ID, id), null);
        c.moveToFirst();
        return new User(c.getLong(0), c.getString(1), c.getString(2), User.Music.values()[c.getInt(3)], (c.getInt(4) != 0));
    }


    public ArrayList<User> getAllUsers() {
        ArrayList<User> users = new ArrayList<>();

        Cursor c = database.rawQuery(String.format("SELECT %s, %s, %s, %s, %s FROM %s",
                COLUMN_USER_ID, COLUMN_FIRST_NAME, COLUMN_EMAIL,
                COLUMN_FAVORITE_MUSIC, COLUMN_ACTIVE, TABLE_NAME), null);
        c.moveToFirst();
        while(!c.isAfterLast()){
            User user = new User(c.getInt(0), c.getString(1), c.getString(2), User.Music.values()[c.getInt(3)], (c.getInt(4) != 0));
            users.add(user);
            c.moveToNext();
        }
        c.close();
        return users;
    }


    public User insertUser(User user){
        ContentValues values = new ContentValues();
        values.put(COLUMN_FIRST_NAME, user.getFirstName());
        values.put(COLUMN_EMAIL, user.getEmail());
        values.put(COLUMN_FAVORITE_MUSIC, user.getFavoriteMusic().ordinal());
        values.put(COLUMN_ACTIVE, (user.isActive() ? 1 : 0));
        long newUserId = database.insert(TABLE_NAME, null, values);

        // Returns NULL if the insert fails..
        if (newUserId != -1) {
            user.setId(newUserId);
            return user;
        } else {
            return null;
        }
    }


    public User updateUser(User user){
        ContentValues values = new ContentValues();
        values.put(COLUMN_FIRST_NAME, user.getFirstName());
        values.put(COLUMN_EMAIL, user.getEmail());
        values.put(COLUMN_FAVORITE_MUSIC, user.getFavoriteMusic().ordinal());
        values.put(COLUMN_ACTIVE, (user.isActive() ? 1 : 0));

        int rowsUpdated = database.update(TABLE_NAME, values, "user_id = " + user.getId(), null);

        // Returns NULL if the insert fails..
        if (rowsUpdated == 1) {
            return user;
        } else {
            return null;
        }
    }


    public int deleteUser(Long id){
        int rowsDeleted = database.delete(TABLE_NAME, COLUMN_USER_ID + " = " + id, null);
        // the above method returns the number of row that were deleted
        return rowsDeleted;
    }
}
