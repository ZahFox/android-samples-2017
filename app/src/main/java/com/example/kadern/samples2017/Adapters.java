package com.example.kadern.samples2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kadern.samples2017.models.User;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Adapters extends AppCompatActivity {

    public static final String TAG = "Adapters";
    ListView listView;
    AppClass app;

    String[] names = {"Bob", "Sally", "Betty"};
    ArrayList<User> users = new ArrayList<User>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapters);

        app = (AppClass)getApplication();
        Toast.makeText(this, app.someGlobalVariable, Toast.LENGTH_SHORT).show();

        listView = (ListView)findViewById(R.id.listView);

        users.add(new User(1, "Bob", "bob@bob.com", User.Music.JAZZ, true));
        users.add(new User(2, "Sally", "sally@sally.com", User.Music.RAP, true));
        users.add(new User(3, "Betty", "betty@betty.com", User.Music.COUNTRY, true));

        //example1(); // Uses an array of strings as the data source
        //example2(); // Uses an ArrayList of users as the data source
        example3(); // Uses a custom view for each item in the users ArrayList
        //example4(); // This is the same as example 3 except it uses a class from a separate file as opposed to an anonymous class


    }

    public void example1() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedString = names[position];
                Log.d(TAG, "You pressed on " + selectedString );
            }
        });
    }

    public void example2() {
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, users);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = users.get(position);
                Log.d(TAG, "You pressed on " + selectedUser.getFirstName() );
            }
        });
    }

    public void example3() {
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, users){
            @Override
            public View getView(int position, View view, ViewGroup parent){
                View itemView = super.getView(position, view, parent);

                TextView lblFirstName = (TextView)itemView.findViewById(R.id.lblFirstName);
                CheckBox chkActive = (CheckBox)itemView.findViewById(R.id.chkActive);

                User u = users.get(position);
                lblFirstName.setText(u.getFirstName());
                chkActive.setChecked(u.isActive());

                chkActive.setTag(u);
                chkActive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckBox pressedCheckBox = (CheckBox)view;
                        User u = (User)pressedCheckBox.getTag();
                        u.setActive(pressedCheckBox.isChecked());
                        Toast.makeText(Adapters.this, u.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

                return itemView;
            }
        };

        listView.setAdapter(adapter);
    }

    public void example4() {
        UserListAdapter adapter = new UserListAdapter(this, R.layout.custom_user_list_item, R.id.lblFirstName, users);
        listView.setAdapter(adapter);
    }
}
