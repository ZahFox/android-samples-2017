package com.example.kadern.samples2017;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class WebServiceActivity extends AppCompatActivity {

    public static final String WEB_SERVICE_URL = "http://wtc-web.com/users-web-service/users/";

    @Override
    public String getSystemServiceName(Class<?> serviceClass) {
        return super.getSystemServiceName(serviceClass);
    }

    TextView txtResponse;
    Button btnGetAllUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service);

        txtResponse = (TextView)findViewById(R.id.txtResponse);
        btnGetAllUsers = (Button)findViewById(R.id.btnGetAllUsers);

        btnGetAllUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    doHttpRequest(WEB_SERVICE_URL);
                } else {
                    Toast.makeText(WebServiceActivity.this, "Network Unavailable", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    private void doHttpRequest(String url) {

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        txtResponse.setText(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(WebServiceActivity.this, "HTTP Error", Toast.LENGTH_LONG).show();
                    }
                }
        );

        queue.add(stringRequest);
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
}
