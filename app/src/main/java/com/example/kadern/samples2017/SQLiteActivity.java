package com.example.kadern.samples2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.kadern.samples2017.dataaccess.ItemDataAccess;
import com.example.kadern.samples2017.models.Item;

import java.util.ArrayList;

public class SQLiteActivity extends AppCompatActivity {

    public static final String TAG = "SQLiteActivity";
    MySQLiteOpenHelper dbHelper;
    ItemDataAccess da;
    ListView itemsListView;
    ArrayList<Item> items;
    Button btnAddItem;
    EditText txtItem;
    ArrayAdapter<Item> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite);

        // Get references to the User Interface
        itemsListView = (ListView)findViewById(R.id.itemsListView);
        btnAddItem = (Button)findViewById(R.id.btnAddItem);
        txtItem = (EditText)findViewById(R.id.txtItem);

        // Initialize the Database
        dbHelper = new MySQLiteOpenHelper(this);

        // Initialize the Item Data Access class
        da = new ItemDataAccess(dbHelper);
        items = da.getAllItems();
        adapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, items);
        itemsListView.setAdapter(adapter);

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str = txtItem.getText().toString();
                if (!str.isEmpty()) {
                    da.insertItem(new Item(0, str));
                    refreshItems();
                    txtItem.setText("");
                } else {
                    Toast.makeText(SQLiteActivity.this, "Please enter an item name", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void refreshItems() {
        adapter.clear();
        items = da.getAllItems();
        adapter.addAll(items);
        adapter.notifyDataSetChanged();
    }
}
