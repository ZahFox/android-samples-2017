package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TempMainActivity extends AppCompatActivity {

    Button btnImgSpinner;
    Button btnIntentSender;
    Button btnAdapters;
    Button btnUserList;
    Button btnActivityLifeCycle;
    Button btnCoderActivity;
    Button btnWebService;

    View.OnClickListener buttonClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent i = null;
            switch (view.getId()) {
                case R.id.btnImgSpinner:
                    i = new Intent(TempMainActivity.this, ImageSpinnerActivity.class);
                    break;
                case R.id.btnIntentSender:
                    i = new Intent(TempMainActivity.this, IntentSenderActivity.class);
                    break;
                case R.id.btnAdapters:
                    i = new Intent(TempMainActivity.this, Adapters.class);
                    break;
                case R.id.btnUserList:
                    i = new Intent(TempMainActivity.this, UserListActivity.class);
                    break;
                case R.id.btnActivityLifecycle:
                    i = new Intent(TempMainActivity.this, LifeCycleActivity.class);
                    break;
                case R.id.btnCoder:
                    i = new Intent(TempMainActivity.this, CoderActivity.class);
                    break;
                case R.id.btnWebService:
                    i = new Intent(TempMainActivity.this, WebServiceActivity.class);
                    break;
            }
            startActivity(i);
        }
    };


    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_temp_main);

        btnImgSpinner = (Button)findViewById(R.id.btnImgSpinner);
        btnIntentSender = (Button)findViewById(R.id.btnIntentSender);
        btnAdapters = (Button)findViewById(R.id.btnAdapters);
        btnUserList = (Button)findViewById(R.id.btnUserList);
        btnActivityLifeCycle = (Button)findViewById(R.id.btnActivityLifecycle);
        btnCoderActivity = (Button)findViewById(R.id.btnCoder);
        btnWebService = (Button)findViewById(R.id.btnWebService);

        btnImgSpinner.setOnClickListener(buttonClickListener);
        btnIntentSender.setOnClickListener(buttonClickListener);
        btnAdapters.setOnClickListener(buttonClickListener);
        btnUserList.setOnClickListener(buttonClickListener);
        btnActivityLifeCycle.setOnClickListener(buttonClickListener);
        btnCoderActivity.setOnClickListener(buttonClickListener);
        btnWebService.setOnClickListener(buttonClickListener);
    }
}
