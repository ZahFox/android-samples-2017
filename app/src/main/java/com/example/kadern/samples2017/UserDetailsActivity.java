package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.kadern.samples2017.dataaccess.UserDataAccess;
import com.example.kadern.samples2017.models.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserDetailsActivity extends AppCompatActivity {

    AppClass app;
    private User user;
    private String mode;

    // This is a regex for validating emails
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    // View References
    EditText txtFirstName;
    EditText txtEmail;
    RadioGroup rgFavoriteMusic;
    CheckBox chkActive;
    Button btnSave;
    Button btnClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        // Reference the View Objects
        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        rgFavoriteMusic = (RadioGroup)findViewById(R.id.rgFavoriteMusic);
        chkActive = (CheckBox)findViewById(R.id.chkActive);
        btnSave = (Button)findViewById(R.id.btnSave);
        btnClear = (Button)findViewById(R.id.btnClear);

        // Bind event listeners to the View Buttons
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUserData();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearDataFromUI();
            }
        });

        // Check the Intent Extras
        Intent i = getIntent();
        Bundle extras = i.getExtras();
        mode = extras.getString(UserListActivity.DETAILS_MODE);
        long userId = extras.getLong(UserListActivity.USER_ID_EXTRA, -1);

        // If user ID was provided, use it to retrieve User data
        if (userId > -1) {
            user = app.userDa.getUser(userId);
            putUserDataInUI();
        } else {
            user = new User();
        }
    }


    private void saveUserData() {
        // Validate UI data before saving..
        if (validateUIData()) {
            // Update the user variable with data from the UI
            setUserDataFromUI();

            // Determine the Mode of the Activity
            User newUserData;
            if (mode.equals("edit")) {
                newUserData = app.userDa.updateUser(user);
            } else {
                newUserData = app.userDa.insertUser(user);
            }

            // Return to the User List Activity if the save was successful
            if (newUserData != null) {
                Intent i = new Intent(UserDetailsActivity.this, UserListActivity.class);
                startActivity(i);
            } else {
                warning("Save Failed. Please try again.");
            }
        }
    }


    private boolean validateUIData() {
        // Validate First Name
        if (txtFirstName.getText().toString().equals("")) {
            warning("Please provide a User name.");
            return false;
        }

        // Validate Email
        String email = txtEmail.getText().toString();
        if (email.equals("")) {
            warning("Please provide a User email.");
            return false;
        } else if (!validateEmail(email)) {
            warning("Please provide a valid User email.");
            return false;
        }

        // Validate Favorite Music
        if (rgFavoriteMusic.getCheckedRadioButtonId() == -1) {
            warning("Please select a music genre.");
            return false;
        }
        return true;
    }


    private void setUserDataFromUI(){
        // Get the First Name
        user.setFirstName(txtFirstName.getText().toString());

        // Get the Email
        user.setEmail(txtEmail.getText().toString());

        // Get the Active Status
        user.setActive(chkActive.isChecked());

        // Get the Favorite Music
        User.Music favoriteMusic = null;
        switch(rgFavoriteMusic.getCheckedRadioButtonId()) {
            case R.id.rbCountry:
                favoriteMusic = User.Music.COUNTRY;
                break;
            case R.id.rbRap:
                favoriteMusic = User.Music.RAP;
                break;
            case R.id.rbJazz:
                favoriteMusic = User.Music.JAZZ;
                break;
        }
        user.setFavoriteMusic(favoriteMusic);
    }


    private void putUserDataInUI() {
        txtFirstName.setText(user.getFirstName());
        txtEmail.setText(user.getEmail());
        chkActive.setChecked(user.isActive());

        switch(user.getFavoriteMusic()) {
            case COUNTRY:
                rgFavoriteMusic.check(R.id.rbCountry);
                break;
            case RAP:
                rgFavoriteMusic.check(R.id.rbRap);
                break;
            case JAZZ:
                rgFavoriteMusic.check(R.id.rbJazz);
                break;
        }
    }


    private void clearDataFromUI() {
        txtFirstName.setText("");
        txtEmail.setText("");
        chkActive.setChecked(false);
        rgFavoriteMusic.clearCheck();
    }


    private void warning(String warning) {
        Toast.makeText(this, warning, Toast.LENGTH_SHORT).show();
    }


    private boolean validateEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }
}
