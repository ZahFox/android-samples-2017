package com.example.kadern.samples2017;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;

/**
 * Created by 2822647 on 10/23/2017.
 */

public class UserListAdapter extends ArrayAdapter {

    ArrayList<User> users;

    public UserListAdapter(@NonNull Context context, @LayoutRes int layoutResourceID, int x, ArrayList<User> users) {
        super(context, layoutResourceID, x, users);
        this.users = users;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        View itemView = super.getView(position, view, parent);
        TextView lblFirstName = (TextView)itemView.findViewById(R.id.lblFirstName);
        CheckBox chkActive = (CheckBox)itemView.findViewById(R.id.chkActive);

        User u = users.get(position);
        lblFirstName.setText(u.getFirstName());
        chkActive.setChecked(u.isActive());

        return itemView;
    }
}
