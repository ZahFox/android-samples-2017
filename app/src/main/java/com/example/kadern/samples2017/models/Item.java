package com.example.kadern.samples2017.models;

/**
 * Created by 2822647 on 11/13/2017.
 */

public class Item {
    private long id;
    private String name;

    public Item(long id, String name){
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return this.name + " (" + this.id + ")";
    }
}

