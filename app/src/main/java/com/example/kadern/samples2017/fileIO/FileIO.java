package com.example.kadern.samples2017.fileIO;

import android.content.Context;
import android.util.Log;

import com.example.kadern.samples2017.AppClass;
import com.example.kadern.samples2017.models.User;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


import static com.example.kadern.samples2017.Adapters.TAG;

/**
 * Created by 2822647 on 10/30/2017.
 */



public class FileIO {


    public void writeUsersToFile(ArrayList<User> users, String filePath, Context c) {
        try {
            FileOutputStream fos = c.openFileOutput(filePath, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(users);
            oos.close();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File Not Found:\n" + e);
        } catch (IOException e) {
            Log.e(TAG, "IO Error:\n" + e);
        } catch (Exception e) {
            Log.e(TAG, "Error:\n" + e);
        }
    }

    public ArrayList<User> readUsersFromFile(String filePath, Context c) {

        ArrayList<User> users = new ArrayList<User>();

        try {
            FileInputStream is = c.openFileInput(filePath);
            ObjectInputStream ois = new ObjectInputStream(is);
            users = (ArrayList<User>)ois.readObject();
            ois.close();
            is.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File Not Found:\n" + e);
        } catch (IOException e) {
            Log.e(TAG, "IO Error:\n" + e);
        } catch (Exception e) {
            Log.e(TAG, "Error:\n" + e);
        }

        return users;
    }
}
